let rows = document.getElementById("rows");
let cols = document.getElementById("cols");
let runButton = document.getElementById("run");
let tableContainer = document.getElementById("tableContainer");

function buildTable() {
    let table = "<table id=table>";
    for (let i = 1; i <= Number(rows.value); i++) {
        table += "<tr>";

        for (let j = 1; j <= Number(cols.value); j++) {
            table += "<td onclick=tablePaint(this)>" + i + j + "</td>";
        }

        table += "</tr>"
    }

    table += "</table>";

    tableContainer.innerHTML = table;
}

runButton.onclick = function() {
    buildTable();

}

function tablePaint(e) {
    if (e.style.backgroundColor == 'rgb(125, 125, 125)') {
        e.style.backgroundColor = 'rgb(255, 255, 255)';
    } else {
        e.style.backgroundColor = 'rgb(125, 125, 125)';
    }


}